package by.epam.training.controllers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Package: by.epam.training.controllers
 * Project name: JspTagApiTask
 * Created by Mikita Isakau
 * Creation date: 19.12.2016
 */

@WebServlet("/main")
public class MainController extends HttpServlet {

    @Override
    protected void doGet(final HttpServletRequest req,
                         final HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().println("I'm in main controller");
    }

    @Override
    protected void doPost(final HttpServletRequest req,
                          final HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
