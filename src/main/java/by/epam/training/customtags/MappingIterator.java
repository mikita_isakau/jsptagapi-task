package by.epam.training.customtags;

import javax.servlet.ServletRegistration;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.*;

/**
 * Package: by.epam.training.customtags
 * Project name: JspTagApiTask
 * Created by Mikita Isakau
 * Creation date: 20.12.2016
 */
public class MappingIterator extends TagSupport {

    private static final String URL_KEY = "url";
    private static final String SERVLET_KEY = "servlet";

    private ServletRequest request;
    private Iterator<String> urlsIterator;

    private void setAttr(final String element) {
        String[] elementSplit = element.split(";");
        request.setAttribute(URL_KEY, elementSplit[0]);
        request.setAttribute(SERVLET_KEY, elementSplit[1]);
    }

    @Override
    public int doAfterBody() throws JspException {
        if (!urlsIterator.hasNext()) {
            return SKIP_BODY;
        }
        setAttr(urlsIterator.next());
        return EVAL_BODY_AGAIN;
    }

    @Override
    public void setPageContext(final PageContext pageContext) {
        super.setPageContext(pageContext);

        request = pageContext.getRequest();
        List<String> urls = new ArrayList<>();
        Iterator<? extends Map.Entry<String, ? extends ServletRegistration>> servletRegistrationIterator = pageContext
                .getServletContext()
                .getServletRegistrations()
                .entrySet()
                .iterator();

        while (servletRegistrationIterator.hasNext()) {
            Map.Entry<String, ? extends ServletRegistration> element = servletRegistrationIterator.next();
            for (String urlMapping : element.getValue().getMappings()) {
                urls.add(urlMapping + ";" + element.getKey());
            }
        }
        urlsIterator = urls.iterator();
    }

    @Override
    public int doStartTag() throws JspException {
        if (Objects.isNull(urlsIterator)) {
            return SKIP_BODY;
        }

        // Show first element
        if (urlsIterator.hasNext()) {
            setAttr(urlsIterator.next());
        }
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doEndTag() throws JspException {
        request.removeAttribute(URL_KEY);
        request.removeAttribute(SERVLET_KEY);
        return super.doEndTag();
    }
}
