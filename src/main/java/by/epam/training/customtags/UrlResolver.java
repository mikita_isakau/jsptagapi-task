package by.epam.training.customtags;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * Package: by.epam.training.customtags
 * Project name: JspTagApiTask
 * Created by Mikita Isakau
 * Creation date: 20.12.2016
 */
public class UrlResolver extends SimpleTagSupport {

    private String url;

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets url.
     *
     * @param url the url
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    private ServletContext getServletContext() {
        return ((PageContext) getJspContext()).getServletContext();
    }

    @Override
    public void doTag() throws JspException, IOException {
        getJspContext().setAttribute("url", url);
        getJspContext().setAttribute("resource", getServletContext().getRealPath(url));
        getJspBody().invoke(null);
    }
}
